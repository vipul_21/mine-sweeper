import React from 'react';
import './NavBar.css'

const navBar = () => {
    return(
        <div className='NavBar'>
            <header>
                MineSweeper
            </header>
        </div>
    )
}

export default navBar;