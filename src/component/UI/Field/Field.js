import React from 'react';
import './Field.css';

const field = (props) => 
    (
        <div className='Field' onClick={props.clickOnField}>
            <p>{props.fieldValue}</p>
        </div>
    )

export default field;