import React from 'react'
import NavBar from '../UI/NavBar/NavBar';
import Board from '../../container/Board/Board'

const layout = () => {
  return(
      <div>
          <header>
              <NavBar></NavBar>
          </header>
          <main>
              <Board></Board>
          </main>
      </div>
  )  
}

export default layout;