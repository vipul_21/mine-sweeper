import React, { Component } from 'react';
import Field from '../../component/UI/Field/Field'
import './Board.css'
import Button from '../../component/UI/Button/Button'

class Board extends Component {
    state = {
         inputValue : [
             [0,0,0,0], [0,0,0,0], [0,0,0,0], [0,0,0,0]
         ]
    }
    clickOnFieldHandler = (row , coloum) => {
        let updateValue = [...this.state.inputValue];
        if(updateValue[row][coloum] === 0){
            updateValue[row][coloum] = 1;
        }else{
            updateValue[row][coloum] = 0;
        }
        this.setState({ 
            inputValue : updateValue
        })
    }

    checkAdjacentMines = (i, j) => {
        let counter = 0;
        let arr = [...this.state.inputValue]
         for(let a = -1; a<=1 ; a++ ){
             for(let b= -1 ; b<=1; b++){
               /*  if(i == 1) {
                    console.log(arr[i+a][j+b], i+a, j+b, a, b, i, j, (i+a>=0 && j+b>=0) && (i+a<4 && j+b<4) && (arr[i+a][j+b] && arr[i+a][j+b] === 1|| arr[i+a][j+b] && arr[i+a][j+b] === 9), counter)
                }  */
                if((i+a>=0 && j+b>=0) && (i+a<4 && j+b<4) && (arr[i+a][j+b] && arr[i+a][j+b] === 1 || arr[i+a][j+b] && arr[i+a][j+b] === 9)){
                    counter = counter +1 ;
                }
             }
         }
        return counter;
    }
    
    clickOnPlay = () => {
        let arr = [...this.state.inputValue]
        let updateArray = []
        this.state.inputValue.map(item => {
            const tempArray = []
            item.map(innerItem => {
                tempArray.push(innerItem)
        })
        updateArray.push(tempArray)
    })
        for(let i in arr){
            for(let j in arr[i])
            {
                console.log('updated array', updateArray)
                if(arr[i][j] === 0){
                  let count = this.checkAdjacentMines(parseInt(i),parseInt(j))
                  updateArray[i][j] = count;
                }else{
                    updateArray[i][j] = 9
            }
            }
        }
        this.setState({
            inputValue: updateArray
        })
    }

    render(){
        let field = this.state.inputValue.map((item, itemIndex) => {
            return(
                <div>
                    {item.map((innerItem, innerIndex) => {
                return(
                        <Field
                    clickOnField = {() => this.clickOnFieldHandler(itemIndex, innerIndex)}
                    fieldValue = {innerItem}
                    />
               )}
            )}
                </div>
            )
     
  })
        return(
            <div className='Board'>
                 {field}
                <Button clicked = {this.clickOnPlay}>PLAY!</Button>

            </div>
        )
    }
}

export default Board;